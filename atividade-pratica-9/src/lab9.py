import re

logs = ""
with open('log9.txt') as f:
  logs = f.read()

recvd_pairs = re.findall(r"Received request 'hello (.+)' from (.+)\n", logs)

last_from_addr = {}
for msgcount, addr in recvd_pairs:
  msgcount = int(msgcount) + 1
  if addr not in last_from_addr or msgcount > last_from_addr[addr]:
    last_from_addr[addr] = msgcount

recvd_from_addr = {}
for _, addr in recvd_pairs:
  if addr not in recvd_from_addr:
    recvd_from_addr[addr] = 0
  recvd_from_addr[addr] += 1

pass