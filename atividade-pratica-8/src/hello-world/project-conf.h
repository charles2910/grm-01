#ifndef PROJECT_CONF_H_
#define PROJECT_CONF_H_

#define LOG_CONF_LEVEL_RPL LOG_LEVEL_DBG

#ifndef RPL_CONF_STATS
#define RPL_CONF_STATS 1
#endif

// Enable OCP
#ifndef RPL_CONF_OF_OCP
#define RPL_CONF_OF_OCP RPL_OCP_OF0 //or RPL_OCP_MRHOF
#endif

#ifndef RPL_CONF_SUPPORTED_OFS
#define RPL_CONF_SUPPORTED_OFS {&rpl_of0, &rpl_mrhof}
#endif

#endif // __PROJECT_CONF_H__
