import mysql.connector
import matplotlib.pyplot as plt

MYSQL_USER = 'root'
MYSQL_PASS = '123teste'

def plotar_resultados(atividade, db):
    conn = mysql.connector.connect(user=MYSQL_USER, password=MYSQL_PASS, host='127.0.0.1', database=db)
    cursor = conn.cursor(dictionary=True)

    cursor.execute("""
        select 
            id_mote,
            avg(latencia_ms) as latencia_media,
            stddev(latencia_ms) as latencia_erro,
            avg(throughput_Bps) as throughput_medio,
            stddev(throughput_Bps) as throughput_erro 
        from metricas_http 
        group by id_mote;
    """)
    agg_http = [dict(x) for x in cursor]

    cursor.execute("""
        select 
            id_mote,
            avg(latencia_ms) as latencia_media,
            stddev(latencia_ms) as latencia_erro,
            avg(throughput_Bps) as throughput_medio,
            stddev(throughput_Bps) as throughput_erro 
        from metricas_coap 
        group by id_mote;
    """)
    agg_coap = [dict(x) for x in cursor]

    fig, axs = plt.subplots(1, 2)

    barwidth = 0.3

    ax = axs[0]
    barras_http = ax.bar([x['id_mote'] for x in agg_http], [x['latencia_media'] for x in agg_http], barwidth, yerr=[x['latencia_erro'] for x in agg_http], error_kw={'lw': 1, 'capsize': 2})
    barras_coap = ax.bar([x['id_mote']+barwidth for x in agg_coap], [x['latencia_media'] for x in agg_coap], barwidth, yerr=[x['latencia_erro'] for x in agg_coap], error_kw={'lw': 1, 'capsize': 2})
    ax.legend((barras_http, barras_coap), ('HTTP', 'CoAP'))
    ax.set_title(f'Latência média - {atividade}')
    ax.set_xlabel('ID do mote')
    ax.set_ylabel('Latência média (ms)')
    ax.set_xticks([x['id_mote'] for x in agg_http])
    ax.set_yscale('log')
    ax.set_axisbelow(True)
    ax.grid(True, 'both', 'y', linestyle='dashed')

    ax = axs[1]
    barras_http = ax.bar([x['id_mote'] for x in agg_http], [x['throughput_medio'] for x in agg_http], barwidth, yerr=[x['throughput_erro'] for x in agg_http], error_kw={'lw': 1, 'capsize': 2})
    barras_coap = ax.bar([x['id_mote']+barwidth for x in agg_coap], [x['throughput_medio'] for x in agg_coap], barwidth, yerr=[x['throughput_erro'] for x in agg_coap], error_kw={'lw': 1, 'capsize': 2})
    ax.legend((barras_http, barras_coap), ('HTTP', 'CoAP'))
    ax.set_title(f'Throughput médio - {atividade}')
    ax.set_xlabel('ID do mote')
    ax.set_ylabel('Throughput médio (B/s)')
    ax.set_xticks([x['id_mote'] for x in agg_http])
    ax.set_yscale('log')
    ax.set_axisbelow(True)
    ax.grid(True, 'both', 'y', linestyle='dashed')


    cursor.close()
    conn.close()

plotar_resultados('Atividade 5', 'redes_moveis_ativ5')
plotar_resultados('Atividade 6', 'redes_moveis_ativ6')

plt.show()