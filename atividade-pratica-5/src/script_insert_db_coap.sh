# Nota: para executar o script deve-se instalar o jq antes: sudo apt-get install jq
IP_motes=($(curl -s 'http://[fd00::201:1:1:1]' | jq -r '."routing-links"[].addr')) # (..) = array
len=${#IP_motes[@]}

user=root
password=123teste
database=redes_moveis_ativ5

for (( i=0; i<${len}; i++ ));
do
  source ./script_measure_coap.sh ${IP_motes[$i]}
  id_mote=$(echo $out | cut -d'_' -f 2)
  mysql --user="$user" --password="$password" --database="$database" --execute="INSERT INTO metricas_coap (id_mote, hora_medicao, latencia_ms, throughput_Bps) VALUES($id_mote, NOW(), $elapsed, $throughput)"
done

