#!/bin/bash
#echo -e "coap-client -m get coap://[$1]/.well-known/core \n"
echo -e "coap-client -m get coap://[$1]/test/hello \n"
start_time=$(date +%s%3N)
#out=$(coap-client -m get coap://[$1]/.well-known/core)
out=$(coap-client -m get coap://[$1]/test/hello)
end_time=$(date +%s%3N)

echo $out

# elapsed time in millisecond
# keep three digits after floating point.
elapsed=$(echo "scale=3; $end_time - $start_time" | bc)
echo -e "\nLatencia: $elapsed ms"

num_bytes=$(echo -n "$out" | wc -c)
#num_kbytes=$(echo "scale=5; $num_bytes/1024" | bc)

echo "Quantidade de dados enviados: $num_bytes Bytes"
throughput=$(echo "scale=2; 1000*$num_bytes/$elapsed" | bc)
echo "Throughput medio: $throughput Bytes/s"
