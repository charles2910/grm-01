x_pos = 31.15
y_pos = 88.85

node_number = 11

avg_speed = 1.5

time_initial = 210.0
time_final = 254.0
step = 0.1

delta_t = time_final - time_initial

time = time_initial
time_steps = []
while time <= time_final:
    time_steps.append(time)
    time  += step

try:
    f = open("positions.txt", mode='w', encoding = 'utf-8')

    for i in time_steps:
        print(node_number, round(i, 2), round(x_pos, 2), round(y_pos, 2), file=f)
        # x_pos += avg_speed * step
        y_pos -= avg_speed * step

finally:
    f.close()
